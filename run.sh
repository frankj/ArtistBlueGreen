#!/usr/bin/env bash

## 该脚本为多命令集中到一起,请拷贝出来单独使用

# STEP-1 docker build,部署到本地
declare -a artist=("jessie" "feng")

for i in "${artist[@]}"
do
    echo "------------------------"
    echo "当前镜像:$i"
    echo -n $i > rank
    make build
    echo "------------------------"
done

# 部署到阿里云仓库
declare -a artist=("jessie" "feng")

for i in "${artist[@]}"
do
    echo "------------------------"
    echo "当前镜像:$i"
    echo -n $i > rank
    make build
    sudo docker tag artist registry.cn-qingdao.aliyuncs.com/${你自己的仓库}/dev:$i
    sudo docker push registry.cn-qingdao.aliyuncs.com/${你自己的仓库}/dev:$i
    echo "------------------------"
done

# STEP-2 apply service
cd k8s/istio
# Blue版本服务发布
kubectl apply -f <(istioctl kube-inject -f artist-blue.yaml)
# kubectl delete -f <(istioctl kube-inject -f artist-blue.yaml)   #若出错,请执行命令

GATEWAY_URL=$(kubectl get po -n istio-system -l istio=ingress -n istio-system -o 'jsonpath={.items[0].status.hostIP}'):$(kubectl get svc istio-ingress -n istio-system -n istio-system -o 'jsonpath={.spec.ports[0].nodePort}')
echo $GATEWAY_URL

# 所有流量指向Blue版本
kubectl apply -f artist-route-blue.yaml

# 新开命令窗口
while true; do curl http://${GATEWAY_URL}/artist/rank; sleep 1; done

# Green版本服务发布
kubectl apply -f <(istioctl kube-inject -f artist-green.yaml)

# 模拟Canary发布
kubectl apply -f artist-route-canary.yaml

# 待新版本稳定之后,将流量切换至Green版本
kubectl apply -f artist-route-green.yaml





FROM alpine:latest
MAINTAINER Frank <xhz7085@163.com>

RUN mkdir -p /app
WORKDIR /app
ADD rank /app/rank
ADD artist /app/artist

CMD ["./artist"]
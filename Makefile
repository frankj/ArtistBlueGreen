IMG := artist
TAG := $(shell cat rank)

build:
	GOOS=linux GOARCH=386 go build
	docker build -t $(IMG):$(TAG) .

run:
	docker run -d -p 6000:6000 $(IMG):$(TAG)

push:
